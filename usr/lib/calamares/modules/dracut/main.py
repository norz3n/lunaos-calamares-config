#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# === This file is part of Calamares - <https://calamares.io> ===
#
#   SPDX-FileCopyrightText: 2014-2015 Philip Müller <philm@manjaro.org>
#   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
#   SPDX-FileCopyrightText: 2017 Alf Gaida <agaid@siduction.org>
#   SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
#   SPDX-FileCopyrightText: 2022 Anke Boersma <demm@kaosx.us>
#   SPDX-License-Identifier: GPL-3.0-or-later
#
#   Calamares is Free Software: see the License-Identifier above.
#
import subprocess
import os
import shutil
import libcalamares
from libcalamares.utils import target_env_process_output


import gettext
_ = gettext.translation("calamares-python",
                        localedir=libcalamares.utils.gettext_path(),
                        languages=libcalamares.utils.gettext_languages(),
                        fallback=True).gettext

def pretty_name():
    return _("Creating initramfs with dracut.")

def run_dracut(installation_root_path):
    kernel_search_path = "/usr/lib/modules"

    # find all the installed kernels and run dracut
    for root, dirs, files in os.walk(os.path.join(installation_root_path, kernel_search_path.lstrip('/'))):
        for file in files:
            if file == "pkgbase":
                kernel_version = os.path.basename(root)
                # run dracut
                pkgbase_location = os.path.join(root, file)
                with open(pkgbase_location, 'r') as pkgbase_file:
                    kernel_suffix = pkgbase_file.read().rstrip()
                try:
                    libcalamares.utils.target_env_process_output(["dracut", "--force", "--hostonly",
                                                                  "--no-hostonly-cmdline",
                                                                  f"/boot/initramfs-{kernel_suffix}.img",
                                                                  kernel_version])
                    libcalamares.utils.target_env_process_output(["dracut", "--force", "--no-hostonly",
                                                                  f"/boot/initramfs-{kernel_suffix}-fallback.img",
                                                                  kernel_version])
                except subprocess.CalledProcessError as cpe:
                    libcalamares.utils.warning(f"dracut failed with error: {cpe.stderr}")

                kernel_name = f"vmlinuz-{kernel_suffix}"
                # copy kernel to boot
                shutil.copy2(os.path.join(root, "vmlinuz"), os.path.join(installation_root_path, "boot", kernel_name))


def run():
    """
    Starts routine to create initramfs. It passes back the exit code
    if it fails.
    """

    try:
        installation_root_path = libcalamares.globalstorage.value("rootMountPoint")
    except KeyError:
        libcalamares.utils.warning('Global storage value "rootMountPoint" missing')

    run_dracut(installation_root_path)
