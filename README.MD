# LunaOS Calamares config

This repository contains all the configurations that are used in LunaOS Calamares installer


## This repository uses code from

- [ArchWiki](https://wiki.archlinux.org/)
- [CachyOS Calamares](https://github.com/CachyOS/cachyos-calamares/tree/cachyos-grub)
- [EndeavourOS Calamares](https://github.com/endeavouros-team/calamares)
